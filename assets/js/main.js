let cards = [
    {nb: 3, value: '1'},
    {nb: 3, value: '2'},
    {nb: 3, value: '14'},
    {nb: 3, value: '15'},
    {nb: 4, value: '3'},
    {nb: 4, value: '13'},
    {nb: 5, value: '4'},
    {nb: 5, value: '12'},
    {nb: 6, value: '5'},
    {nb: 6, value: '11'},
    {nb: 7, value: '6'},
    {nb: 7, value: '10'},
    {nb: 8, value: '7'},
    {nb: 8, value: '9'},
    {nb: 0, value: '8'}
];

let item = [
    { nb: 9, value: 'Fabricant de piscine', src: "./assets/img/piscine.png" },
    { nb: 9, value: "Agence d'interim", src: "./assets/img/agence_interim.png" },
    { nb: 9, value: 'Numero Bis', src: "./assets/img/numero_bis.png" },
    { nb: 18, value: 'Paysagiste', src: "./assets/img/paysagiste.png" },
    { nb: 18, value: 'Agent immobilier', src: "./assets/img/agent_immobilier.png" },
    { nb: 18, value: 'Geometre', src: "./assets/img/geometre.png" }
];

/*créer un tableau avec la valeur de toutes les cartes*/
let array_cards = [];
for(let i = 0; i < cards.length; i++)
{

    for (j = 0; j < cards[i].nb; j++)
    {
        array_cards.push(cards[i].value);
    }

}
let array_item = [];
for(let i = 0; i < item.length; i++)
{

    for (j = 0; j < item[i].nb; j++)
    {
        array_item.push(item[i]);
    }
}

/*créer un tableau avec les cartes mélangées*/
let array_cards_melange = [];
for(let i = 0; i < array_cards.length;)
{
    let alea = Math.floor(Math.random()*array_cards.length);
    array_cards_melange.push(array_cards[alea]);
    array_cards.splice(alea, 1);
}
let array_item_melange = [];
for(let i = 0; i < array_item.length;)
{
    let alea = Math.floor(Math.random()*array_item.length);
    array_item_melange.push(array_item[alea]);
    array_item.splice(alea, 1);
}

/*affiche les cartes et les mets dans la fausse*/
let changeCardsButton = document.getElementById('changeCardsButton');

let ca1 = document.getElementById('ca1-p');
let ca2 = document.getElementById('ca2-p');
let ca3 = document.getElementById('ca3-p');

let ca4 = document.getElementById('ca4-img');
let ca4p = document.getElementById('ca4-p');

let ca5 = document.getElementById('ca5-img');
let ca5p = document.getElementById('ca5-p');

let ca6 = document.getElementById('ca6-img');
let ca6p = document.getElementById('ca6-p');

let fosse_item = [];
let fosse_cards = [];

function changeCard() {
    if (array_cards_melange.length >= 1) {
        ca1.innerHTML = array_cards_melange[0];
        ca2.innerHTML = array_cards_melange[1];
        ca3.innerHTML = array_cards_melange[2];
    
        fosse_cards.push(array_cards_melange[0], array_cards_melange[1], array_cards_melange[2]);
        array_cards_melange.splice(0, 3);
    } else {
        ca1.innerHTML = "";
        ca2.innerHTML = "";
        ca3.innerHTML = "";
    }
    
    if (array_item_melange.length >= 1) {
        ca4.src = array_item_melange[0].src;
        ca4p.innerHTML = array_item_melange[0].value;

        ca5.src = array_item_melange[1].src;
        ca5p.innerHTML = array_item_melange[1].value;

        ca6.src = array_item_melange[2].src;
        ca6p.innerHTML = array_item_melange[2].value;

        fosse_item.push(array_item_melange[0], array_item_melange[1], array_item_melange[2]);
        array_item_melange.splice(0, 3);
    } else {
        ca4.src = "";
        ca4p.innerHTML = "";

        ca5.src = "";
        ca5p.innerHTML = "";

        ca6.src = "";
        ca6p.innerHTML = "";
    }
};